﻿using Microsoft.OpenApi.Models;
using System.Collections.Generic;

namespace Demo.Api.Config
{
    public class OAuth2Scheme : OpenApiSecurityScheme
    {
        public string Flow { get; set; }
        public string AuthorizationUrl { get; set; }
        public Dictionary<string, string> Scopes { get; set; }
    }
}
