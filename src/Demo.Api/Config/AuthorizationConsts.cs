﻿namespace Demo.Api.Config
{
    public class AuthorizationConsts
    {
        public const string IdentityServerBaseUrl = "https://localhost:44310";
        public const string OidcSwaggerUIClientId = "core_api_swaggerui";
        public const string OidcApiName = "core_api";

        public const string AdministrationPolicy = "RequireAdministratorRole";
        public const string AdministrationRole = "SkorubaIdentityAdminAdministrator";

        public const string ApiName = "core_api";

        public const string ApiVersionV1 = "v1";

        public const string ApiBaseUrl = "https://localhost:44305";

    }
}