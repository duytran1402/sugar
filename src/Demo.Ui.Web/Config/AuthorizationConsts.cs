﻿namespace Demo.Ui.Web.Config
{
    public class AuthorizationConsts
    {
        public const string IdentityServerBaseUrl = "https://localhost:44310";
        public const string IdentityAdminCookieName = "IdentityServerAdmin";
        public const string OidcSwaggerUIClientId = "core_api_swaggerui";
        public const string OidcApiName = "core_api";

        public const string AdministrationPolicy = "RequireAdministratorRole";
        public const string AdministrationRole = "SkorubaIdentityAdminAdministrator";

        public const string ApiName = "Chopper Api";

        public const string ApiVersionV1 = "v1";

        public const string ApiBaseUrl = "https://localhost:44305";
        public const string SignInScheme = "Cookies";
        public const string OidcAuthenticationScheme = "oidc";
        public const string AccountLoginPage = "Account/Login";


    }
}